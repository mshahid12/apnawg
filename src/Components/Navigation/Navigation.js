import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { NavLink, Router, Switch, Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import './Navigation.css';

const Navigation = ( props ) => {
    return (
        <div className="container">
            <Button component={Link} to="/news" variant="contained" fullWidth={true}>
                News
            </Button>
            <Button component={Link} to="/wg" variant="contained" color="primary" fullWidth={true}>
                WG / Wohnung
            </Button>
            <Button component={Link} to="/fav" variant="contained" color="secondary" fullWidth={true}>
                Favourites
            </Button>
            <Button component={Link} to="/words" variant="contained" fullWidth={true}>
                Words
            </Button>
        </div>
    );
};


export default Navigation;