import React from 'react';
import HeaderAppBar from './HeaderAppBar';

const Header = function(){
        return (
            <div>
                <HeaderAppBar />
            </div>
        );
};

export default Header;