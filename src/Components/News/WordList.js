import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {dict} from '../../dictionaryAssets/dictionary';
import {stopWords} from '../../dictionaryAssets/stopWords';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});

let id = 0;
function createData(name, calories, fat, carbs, protein) {
    id += 1;
    return { id, name, calories, fat, carbs, protein };
}

const rows = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
];

const processWords = (words, stopWords) => {
    //process stop words
};

function SimpleTable(props) {
    const { classes, words } = props;

    let processedWords = processWords(words, stopWords);

    console.log(processedWords);

    return (
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell>German Words</TableCell>
                        <TableCell align="right">English Meaning</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>

                    {words.map((row,index) => (
                        <TableRow key={index}>
                            <TableCell component="th" scope="row">
                                {
                                   row
                                }
                            </TableCell>
                            <TableCell align="right">{dict[row]}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </Paper>
    );
}

SimpleTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleTable);