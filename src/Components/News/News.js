import React, { Component } from 'react';
import {newsApikey} from './keys';
import ArticlesBox from './ArticlesBox';
import axios from 'axios';

class News extends Component {

    constructor(props){
        super();
        this.state=({
            headlines: []
        });
    }

    componentDidMount() {
        this.fetchNewsData();
    }

    componentWillUnmount() {
        this.setState({ headlines: [] });
    }

    fetchNewsData(){
        axios.get(`https://newsapi.org/v2/everything?sources=die-zeit&apiKey=`+ newsApikey.NewsApi)
             .then(res => {
                 const headlines = res.data;
                 this.setState({ headlines: headlines.articles });
             })
    }

    render() {
        return (
            <div>
                {
                    this.state.headlines.map((article, index) => {
                        if(article.title !== null){
                            return <ArticlesBox articles={article} key={index} />;
                        }
                    } )
                }
            </div>
        );
    }
}

export default News;