import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Tooltip from '@material-ui/core/Tooltip';
import WordList from './WordList';

import './news.css';

const styles = theme => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem( 15 ),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem( 15 ),
        color: theme.palette.text.secondary,
    },
    fab: {
        margin: theme.spacing.unit * 2,
    },
    absolute: {
        position: 'absolute',
        bottom: theme.spacing.unit * 2,
        right: theme.spacing.unit * 3,
    }
});

class ArticlesBox extends React.Component {

    constructor( props ) {
        super( props );
        var wordArr = [];
        this.state = {
            words: []
        };
    }

    state = {
        expanded: null,
        wordList: false
    };

    handleChange = panel => ( event, expanded ) => {
        this.setState( {
            expanded: expanded ? panel : false,
        } );
        this.setState({
            wordList:true
        });
        if( this.state.expanded ) {
            //executed when panel closed
        }
    };

    componentDidMount() {
        var description = this.props.articles.description === null ? '' : this.props.articles.description.split( ' ' );
        this.processWords(description);
    }

    //Will rethink about the idea
    processWords = (words) => {
        this.setState({
            words: words.map((word) => {
                return (word.replace(/[^A-Züö0-9]+/ig, ""));
            })
        });
    };


    render() {
        const { classes } = this.props;
        const { expanded } = this.state;

        return (
            <div className={classes.root}>
                <ExpansionPanel expanded={expanded === 'panel1'} onChange={this.handleChange( 'panel1' )}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
                        <Typography component={'span'} className={classes.heading}><h2>{this.props.articles.title}</h2>

                        </Typography>
                        {/*<Typography className={classes.secondaryHeading}>{this.props.articles}</Typography>*/}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <div id="wrapper">
                            <div id="first">
                                <Typography component={'span'}>
                                    <h3>{this.props.articles.description}</h3>
                                </Typography>
                            </div>
                            <div id="second">
                                <img src={this.props.articles.urlToImage} className="imgNews"/>
                            </div>
                            {this.state.wordList ? <WordList words={this.state.words}/> : ''}
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        );
    }
}

ArticlesBox.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles( styles )( ArticlesBox );