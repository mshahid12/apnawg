import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect,
    BrowserRouter
} from "react-router-dom";

import "../App.css";

import Header from '../Components/Header/Header';
import Navigation from '../Components/Navigation/Navigation';
import News from '../Components/News/News';
import WG from '../Components/WG/Wg';
import Fav from '../Components/Favourites/Fav';
import WordList from '../Components/WordList/WordList';

function AppRouter() {
    return (
        <div>
            <BrowserRouter>
                <Header/>
                <Navigation/>
                <Route exact path="/news" component={News}/>
                <Route exact path="/wg" component={WG}/>
                <Route exact path="/fav" component={Fav}/>
                <Route exact path="/words" component={WordList}/>
            </BrowserRouter>
        </div>
    );
}

export default AppRouter;